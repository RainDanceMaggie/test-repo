BrowserStack - Bitbucket Pipelines
====================================

Example demonstrating the execution of Selenium tests on [BrowserStack Automate] with Bitbucket Pipelines.

* A simple web server (Sinatra) is started on port 8001 to serve two pages at `/` and `/welcome`
* To enable remote browsers and devices on BrowserStack to access the local server [BrowserStack Local] is downloaded and used to establish a secure connection to `BrowserStack` servers
* An RSpec test defined in `spec/welcome.rb` and it runs Selenium tests on [BrowserStack Automate]


## Usage

* Clone this repository or create a fork:
  - `git clone https://bitbucket.org/browserstack/bitbucket-pipelines.git`

* Add the following environment variables to your Bitbucket repository:
  - `BROWSERSTACK_USER`: (Required) BrowserStack username
  - `BROWSERSTACK_ACCESSKEY`: (Required) BrowserStack access key

  Note: You can get these values from BrowserStack [Account Settings].


## Build Configuration

```
image: ruby:2.1
pipelines:
  default:
    - step:
        script:
          - apt-get update
          - apt-get install unzip
          - bundle install
          - bundle exec rackup -p 8001 > /dev/null &
          - ./run_local.bash
          - bundle exec rspec spec/welcome.rb
          - pkill -9 rackup
```

* Sets the Docker image to be used for builds to Ruby 2.1
* Installs unzip utility for extracting `.zip` files
* Installs Ruby gems required by this example
* Starts Sinatra web server on port 8001
* Runs a bash script that downloads, extracts, and installs [BrowserStack Local]
* Runs the Selenium test cases defined in `spec/welcome.rb`
* Terminates the web server

[BrowserStack Local]:https://www.browserstack.com/local-testing
[BrowserStack Automate]:https://www.browserstack.com/automate
[Account Settings]:https://www.browserstack.com/accounts/settings
