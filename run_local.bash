#!/bin/bash -e

wget http://www.browserstack.com/browserstack-local/BrowserStackLocal-linux-x64.zip
unzip BrowserStackLocal-linux-x64.zip
./BrowserStackLocal ${BROWSERSTACK_ACCESSKEY} localhost,8001,0 > /dev/null &
sleep 10
